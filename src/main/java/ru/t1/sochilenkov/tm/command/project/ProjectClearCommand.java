package ru.t1.sochilenkov.tm.command.project;

public final class ProjectClearCommand extends AbstractProjectCommand {

    public static final String DESCRIPTION = "Clear project list.";

    public static final String NAME = "project-clear";

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR PROJECTS]");
        getProjectService().clear();
    }

}
